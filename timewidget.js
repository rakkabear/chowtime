//Widget that shows Restaurant Hours and current status

var dayOfWeek = (new Date).getDay(); //returns an integer repr. day of week (0 = sunday, 1 = monday, etc.)
var hours = [ "Closed", //Sunday
			"10 AM to 3 PM",   // Monday
            "11 AM to 4 PM",   // Tuesday
            "12 AM to 1 PM",   // Wednesday
            "11 AM to 2 PM",   // Thursday
            "10 AM to 7 PM",   // Friday
            "12 PM to 6 PM"];  // Saturday

var todaysHours = hours[dayOfWeek];
document.getElementById("hours").innerHTML = todaysHours;


/*ourDate = new Date();
document.write("The time and date at your computer's location is: "
+ ourDate.toLocaleString() 
+ ".<br/>");
document.write("The time zone offset between local time and GMT is " 
+ ourDate.getTimezoneOffset() 
+ " minutes.<br/>");
document.write("The time and date (GMT) is: " 
+ ourDate.toGMTString() 
+ ".<br/>");*/
